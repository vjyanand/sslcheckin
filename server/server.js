import webpack from 'webpack'
import express from 'express';
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import config from '../webpack.config.dev'
import path from 'path';
import favicon from 'serve-favicon';
import sslRoutes from './routes/ssl.routes';
import serverConfig from './config';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import expressSession from 'express-session';
import flash from 'connect-flash';
import passport from 'passport';
import { Strategy as FacebookStrategy} from 'passport-facebook';
import { Strategy as TwitterStrategy} from 'passport-twitter';
import { Strategy as GoogleStrategy} from 'passport-google-oauth2';
import { updateUserPlain as updateUser } from '../common/actions'

const app = new express();

if (process.env.NODE_ENV !== 'production') {
  const compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
  app.use(webpackHotMiddleware(compiler));
}

app.use(flash());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

app.use(cookieParser());
app.use(expressSession({secret:'l6g7F93K27G2818P', resave:true, saveUninitialized: true}));

app.use(passport.initialize());
app.use(passport.session());

app.get("/auth/facebook", passport.authenticate("facebook", { scope : "email"}));
app.get('/auth/twitter', passport.authenticate('twitter'));
app.get('/auth/google', passport.authenticate('google', { scope: [ 'https://www.googleapis.com/auth/plus.profile.emails.read' ] } ));

app.get('/auth/facebook/callback', passport.authenticate('facebook', { successRedirect: '/user', failureRedirect: '/', failureFlash: true }));
app.get('/auth/twitter/callback', passport.authenticate('twitter', { successRedirect: '/user', failureRedirect: '/' }));
app.get('/auth/google/callback', passport.authenticate('google', { successRedirect: '/user', failureRedirect: '/' }));

passport.use(new FacebookStrategy({
    clientID: "1697201700560745",
    clientSecret: "bb332e542b2d0b17254ce10ad8a3f5fe",
    callbackURL: "http://localhost:8000/auth/facebook/callback",
    profileFields: ['id', 'email', 'name']
  },
  function(accessToken, refreshToken, profile, done) {
    let result = { provider:profile.provider, id:profile.id, name:profile.name.givenName, email:profile.emails[0].value}
    updateUser(result, done);
  }
));

passport.use(new TwitterStrategy({
    consumerKey: "WQScKgLI63KDHdL4V9jacQNq4",
    consumerSecret: "2JQIx8ty58KvZ7cPqS87FBM2kwTCYpVsHgY7CgXKrGsc8LiPJn",
    callbackURL: "http://localhost:8000/auth/twitter/callback",
    userProfileURL: "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true"
  },
  function(accessToken, refreshToken, profile, done) {
    let result = { provider:profile.provider, id:profile.id, name:profile.displayName, email:profile.emails[0].value}
    updateUser(result, done);
  }
));

passport.use(new GoogleStrategy({
    clientID: "783017582460-9t8207cai624b8t4uoghs3bhsi7dvdcp.apps.googleusercontent.com",
    clientSecret: "gAs3JCkaP_02dIuE-cNljapC",
    callbackURL: "http://localhost:8000/auth/google/callback",
    passReqToCallback: true
  },
  function(request, accessToken, refreshToken, profile, done) {
    let result = { provider:profile.provider, id:profile.id, name:profile.displayName, email:profile.emails[0].value}
    updateUser(result, done);
  }
));

passport.serializeUser(function(user, done) {
  console.log('serializeUser: ' + JSON.stringify(user))
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  console.log('DeserializeUser: ' + JSON.stringify(user))
    done(null, user);
});

app.use(express.static(path.resolve(__dirname, '../static')));

app.use('/api', sslRoutes);
app.use(favicon(path.resolve(__dirname, '../static/fav/favicon.ico')));

app.get("/", function(req, res) {
  res.sendFile(path.resolve(__dirname, '../static/index.html'))
})

// start app
app.listen(serverConfig.port, (error) => {
  if (!error) {
    console.log(`App is running on port: ${serverConfig.port}!`); // eslint-disable-line
  }
});

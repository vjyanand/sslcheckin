import pg from 'pg';
import config from '../config';
import sanitizeHtml from 'sanitize-html';
import tls from 'tls';

const conString = config.pgURL || "postgres://username:password@localhost/database";

export function updateUser(req, res) {
  pg.connect(conString, function(err, client, done) {
    if (err) {
      return res.status(500).end();
    }

    let body = req.body;
    let provider = body.provider.toUpperCase();
    let provider_id = body.id;
    let provider_email = body.email;
    let provider_name = body.name;
    let sql = 'INSERT INTO users (provider, provider_id, provider_email, provider_name) values (\'' + provider + '\', \'' + provider_id + '\', \'' + provider_email + '\', \'' + provider_name + '\') ON CONFLICT (provider,provider_id,provider_email) DO UPDATE SET updated_login = current_timestamp, provider_name = \'' + provider_name + '\''
    client.query(sql, function(err, result) {
      done();
      if (err) {
        return res.status(500).end();
      }
      return res.json({
        scoops: result.rows
      });
    });
  });
}

export function tlsInfo(options, cb) {
  if (!options.hasOwnProperty('rejectUnauthorized')) {
      options['rejectUnauthorized'] =  false;
  }
  if (!options.hasOwnProperty('servername')) {
      options['servername'] = options['host'];
  }
  if (!options.hasOwnProperty('port')) {
      options['port'] = 443;
  }

  let tlsSocket = tls.connect(options, function(p) {
    let subject = tlsSocket.getPeerCertificate(false);
    let keyInfo = tlsSocket.getEphemeralKeyInfo();
    let cipher = tlsSocket.getCipher();
    let response = {subject:subject, keyInfo:keyInfo,cipher:cipher}
    return cb(JSON.stringify(response));
  });

  tlsSocket.on('error', function(err) {
    return cb(err);
  });

}

export function tlsInfoforHost(req, res) {
  let options = req.body;
  console.log(options);
  tlsInfo(options, function(result) {
    console.log(result);
    return res.json({});
  });
}

import Express from 'express';
import * as SslController from '../controllers/ssl.controller';
const router = Express.Router();

//Update Scoop
router.route('/user').post(SslController.updateUser);
router.route('/tls').post(SslController.tlsInfoforHost);

export default router;

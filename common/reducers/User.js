
import * as Actions from '../actions'

function user(state = {showLoginModal:false, isLoggedin: false, user: null }, action) {
  switch (action.type) {
    case Actions.LOGIN:
      return Object.assign({}, state, {showLoginModal: showLoginModal});
    case Actions.TOGGLE_LOGIN_MODAL:
        let showLoginModal = !state.showLoginModal
        return Object.assign({}, state, {showLoginModal: showLoginModal});  
    default:
      return state
  }
}

export default user

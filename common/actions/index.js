import fetch from 'isomorphic-fetch'

export const TOGGLE_LOGIN_MODAL = 'TOGGLE_LOGIN_MODAL'

const baseURL = typeof window === 'undefined' ? process.env.BASE_URL || (`http://localhost:${(process.env.PORT || 8000)}`) : '';

export function toggleLoginModal() {
  return {
    type: TOGGLE_LOGIN_MODAL
  }
}

export function updateUserPlain(user, done) {
  fetch(`${baseURL}/api/user`, {'method':'POST', 'headers':{'content-type': 'application/json'}, 'body': JSON.stringify(user)})
      .then(response => response.json())
      .then(json => {
        done(null, user);
      })
      .catch(err => {done(err, user);});
}

export function updateUser(user) {
  console.log(JSON.stringify(user) + "B");
  return (dispatch, getState) => {
    return fetch(`${baseURL}/api/user`, {'method':'POST', 'headers':{'content-type': 'application/json'}, 'body': JSON.stringify(user)})
      .then(response => response.json())
      .then(json => {

      })
      .catch(err => {throw err;});
  };
}

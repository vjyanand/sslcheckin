import { Link } from 'react-router';
import React, { Component, PropTypes } from 'react';

class Footer extends Component {

  render() {
    return (
      <footer className="footer">
        <div className="container">
          <span className="text-muted text-center">Place sticky footer content here.</span>
        </div>
      </footer>
   );
  }
}

export default Footer;

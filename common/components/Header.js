import { Link } from 'react-router';
import React, { Component, PropTypes } from 'react';

class Header extends Component {

  render() {
    return (
        <div className="header">
          <div className="header-content row">
            <div className="col-md-8 col-xs-8">
              <Link className="site-logo" to="/" onClick={this.props.handleLogoClick}></Link>
            </div>
            <div className="col-md-4 col-xs-4">
              
              <button type="button" className="btn-sm signin pull-right btn btn-primary" onClick={this.props.toggleLogin}>Signin</button>
            </div>
          </div>
        </div>

   );
  }
}

export default Header;

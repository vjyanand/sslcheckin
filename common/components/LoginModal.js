import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom'

class LoginModal extends Component {
  componentDidMount() {
    $(ReactDOM.findDOMNode(this)).modal('show');
    $(ReactDOM.findDOMNode(this)).on('hidden.bs.modal', this.props.handleHideModal);
  }

  render() {
    return(
      <div className="modal">
        <div className="modal-dialog modal-sm1">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
              <h4 className="modal-title">Signin</h4>
            </div>

            <div className="modal-body">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-6 col-md-offset-3">
                    <a className='btn btn-danger disabled'>
                      <i className="fa fa-google-plus"></i>
                    </a>
                    <a className='btn btn-danger' href="/auth/google">Sign in with Google</a>
                  </div>
                </div>
                <div className="row">&nbsp;</div>
                <div className="row">
                  <div className="col-md-6 col-md-offset-3">
                    <a className='btn btn-primary disabled'>
                      <i className="fa fa-facebook"></i>
                    </a>
                    <a className='btn btn-primary' href="/auth/facebook">Sign in with Facebook</a>
                  </div>
                </div>
                <div className="row">&nbsp;</div>
                <div className="row">
                  <div className="col-md-6 col-md-offset-3">
                    <a className='btn btn-info disabled'>
                      <i className="fa fa-twitter"></i>
                    </a>
                    <a className='btn btn-info' href="/auth/twitter">Sign in with Twitter</a>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default LoginModal;

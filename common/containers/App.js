import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Header from '../components/Header';
import Footer from '../components/Footer';
import LoginModal from '../components/LoginModal';

import * as Actions from '../actions'

class App extends Component {

  componentDidMount() {
    const { dispatch } = this.props
  }

  toggleLoginModal() {
    const { dispatch } = this.props
    dispatch(Actions.toggleLoginModal())
  }

  handleHideModal() {
    const { dispatch } = this.props
    dispatch(Actions.toggleLoginModal())
  }

  render() {
    const { User } = this.props
    return (
      <div>
        <Header isLoggedin={User.isLoggedin} toggleLogin={this.toggleLoginModal.bind(this)}/>
        { User.showLoginModal &&
          <LoginModal handleHideModal={this.handleHideModal.bind(this)}/>
        }
        <Footer/>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired
}


export default connect(mapStateToProps)(App)
